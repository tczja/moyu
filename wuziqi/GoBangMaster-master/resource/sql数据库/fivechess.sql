/*
Navicat MySQL Data Transfer

Source Server         : mashuang
Source Server Version : 50726
Source Host           : 47.94.167.23:3306
Source Database       : financial

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-12-27 15:12:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE DATABASE IF NOT EXISTS financial
DEFAULT CHARACTER SET utf8mb4 -- UTF-8 Unicode
DEFAULT COLLATE utf8mb4_general_ci;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `name` varchar(255) DEFAULT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `winNum` int(11) DEFAULT NULL,
  `loseNum` int(11) DEFAULT NULL,
  `tiedNum` int(11) DEFAULT NULL,
  `userId` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
