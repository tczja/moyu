package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import entity.User;
/**
 * 功能: User类接口实现
 * @author Administrator
 */
public class IUserDaoImp implements IUserDao {
	String sql = null;
	BaseDao b = new BaseDao();
    /**
    * 功能: 根据姓名查找该玩家信息
    * @param userName 玩家姓名
    * @return 返回实体类供他人调用
    */
	@Override
	public
	 User findUser(String userName){
		sql="select * from user where name=?";
		String[] paras = {userName};
		ResultSet r = BaseDao.doQuery(sql, paras);
		User u = null;
		try {
			if (null !=r && r.next()) {
				u = new User(r.getString(2),
						r.getString(3),
						Integer.parseInt(r.getString(4)),
						Integer.parseInt(r.getString(5)),
						Integer.parseInt(r.getString(6)));
				return u;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;	
	}
    /**
    * 功能: 查找玩家信息
    * @return 返回实体类供他人调用
    */
	@Override
	public List<User> findAll() {
		String sql = "select * from user";
		ResultSet rs = BaseDao.doQuery(sql);
		ArrayList<User> list = new ArrayList<>();
		User u;
		try {
			if (null !=rs && rs.next()) {
				u = new User(rs.getString(2),
						rs.getString(3),
						Integer.parseInt(rs.getString(4)),
						Integer.parseInt(rs.getString(5)),
						Integer.parseInt(rs.getString(6)));
                list.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	  	 return list;
	}
	
	
    /**
    * 功能: 设置玩家的头像
    * @param newFileName  头像路径
    */
	@Override
	public void updateUserImag(String newFileName,String uName) {
		sql="update user set fileName=?  where name=? ";
		String[] paras = {newFileName, uName};
		BaseDao.doUpdate(sql, paras);
	}

	
    /**
    * 功能:删除玩家
    * @param userName 玩家姓名
    */
	@Override
	public void deleteUser(String userName) {
		sql="delete from user where name=?";
		String[] paras={userName};
		BaseDao.doUpdate(sql, paras);
	}

    /**
    * 功能：查找战绩
    * @param userName 玩家姓名
    * @return 战绩
    */
	@Override
	public int findWinNum(String userName) {
		sql="select * from user where name=?";
		String[] paras = {userName};
		int	t = 1;
		ResultSet rs = BaseDao.doQuery(sql, paras);
		User u;
		try {
			while(rs.next()) {
				u = new User(rs.getString("name"),rs.getString("fileName"),Integer.parseInt(rs.getString("winNum")),Integer.parseInt(rs.getString("loseNum")),Integer.parseInt(rs.getString("tiedNum")));
			    t = u.getWinNum();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			t = 0;
		}
	    return t;
	}

    /**
    * 功能：修改战绩
    * @param newWinNum 战绩
    */
	@Override
	public
	  void update(int newWinNum,String name) {
		sql = "update user set winNum=? where name=?";
		String[] paras = {newWinNum+"",name};
		BaseDao.doUpdate(sql, paras);
	}

	@Override
	public void insertUser(User user){
		sql = "insert into user(name,winNum,loseNum,tiedNum,createDate) values(?,?,?,?,?)";
		System.out.println("新增用户");
		String[] paras = {user.getName(),0+"",0+"",0+"",now()};
		b.doInsert(sql,paras);
	}
	@Override
	public void updateLoseNum(int loseNum,String name){
		sql="update user set loseNum=? where name=?";
		String[] paras={loseNum+"",name};
		BaseDao.doUpdate(sql, paras);
	}

    /**
     * 获取当前时间的字符串
     * @return
     */
	private String now() {
		return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now());
	}
}
	
  