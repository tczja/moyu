package consts;

/**
 * @author Administrator
 */
public class Consts {
    /** 用户管理页面跳转地址 */
    public static final String USER_INFO_URL = "cmd   /c   start   http://82.157.31.37:9999/userinfo.html";
    /** 战绩查询页面跳转地址 */
    public static final String RECORD_QUERY_URL = "cmd   /c   start   http://82.157.31.37:9999/record.html";
    /** 数据库连接url */
    public static final String DATA_SOURCES_URL = "jdbc:mysql://82.157.31.37:3306/gobang?useSSL=false";
    /** 数据库连接username */
    public static final String DATA_SOURCES_USERNAME = "root";
    /** 数据库连接password */
    public static final String DATA_SOURCES_PASSWORD = "Jd@@89461418";
    /** 数据库连接classname */
    public static final String DATA_SOURCES_CLASSNAME = "com.mysql.jdbc.Driver";
    /** 五子棋难度等级 */
    public static final String LEVEL_1 = "低";
    public static final String LEVEL_2 = "中";
    public static final String LEVEL_3 = "高";
    /** 五子棋难度等级对应的AI评分参数 */
    public static final int[] LEVEL_1_PARAM = new int[] {6,12,25,40,5,10,20,30};
    public static final int[] LEVEL_2_PARAM = new int[] {5,25,80,100,5,25,80,100};
    public static final int[] LEVEL_3_PARAM = new int[] {5,50,140,400,5,52,150,410};

}
