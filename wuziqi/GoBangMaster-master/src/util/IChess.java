package util;

/**
 * 功能: 五子棋规则接口
 * @author Administrator
 */
public interface IChess {
  /**
   * 功能: 在棋盘上添加棋子
   * @param x  横坐标
   * @param y  纵坐标
   * @param type  1:白棋   2:黑棋
   * @return
   */
  boolean add(int x,int y,int type);

  /**
   * 功能: 悔棋时删除棋盘上的棋子
   * @param type  1:白棋   2:黑棋
   */
  void delete(int type);

  /**
   * 功能: 判断是否连成五子
   * @param x  横坐标
   * @param y  纵坐标
   * @param type  1:白棋   2:黑棋
   * @return
   */
  boolean compare(int x,int y,int type);

  /**
   * 功能：初始化棋盘，默认棋牌每个位置初始值为：0；
   */
  void resetGame();

  /**
   * 功能：人机对战时初始化棋盘，默认棋牌每个位置初始值为：0，并设置难度等级
   * @param level 难度等级
   */
  void resetGame(int level);

  /**
   * 功能：判断五子棋人机电脑默认为白方，制定白方下棋规则，给定白子位置并以返回白子坐标
   * @param x 横坐标
   * @param y 纵坐标
   * @return
   */
  int [] comTurn(int x, int y);


}
